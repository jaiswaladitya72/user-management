import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private userData = new BehaviorSubject<any>(null);

  constructor(private http : HttpClient) { }
  public loginUserName = sessionStorage.getItem('LoginUserName');

  logOutUser(): void {
    sessionStorage.clear();
  }

  getUsers(data?:any){
    let k = data ? data : ''
    return this.http.get<any>(`http://localhost:3000/posts/${k}`)
    .pipe(map((res:any)=>{
      return res
    }))
  }

  setUserData(data: object){
    this.userData.next(data);
  }

  getUserData(){
    return this.userData.value;
  }

  postUser(data: any){
    const headerData: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'calling_entity': 'UI',
      'UserName':this.loginUserName
    });
    return this.http.post<any>("http://localhost:3000/posts",data,{headers: headerData})
    .pipe(map((res:any)=>{
      return res
    }))
  }

  updateUser(data: any, id: number){
    const headerData: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'calling_entity': 'UI',
      'UserName':this.loginUserName
    });
    return this.http.put<any>("http://localhost:3000/posts/"+id,data,{headers: headerData})
    .pipe(map((res:any)=>{
      return res
    }))
  }
  
  deleteUser(id: number){
    const headerData: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'calling_entity': 'UI',
      'UserName':this.loginUserName
    });
    return this.http.delete<any>("http://localhost:3000/posts/"+id,{headers: headerData})
    .pipe(map((res: any)=>{
      return res
    }))
  }

}
