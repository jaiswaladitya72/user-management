import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-login-dashboard',
  templateUrl: './login-dashboard.component.html',
  styleUrls: ['./login-dashboard.component.scss']
})
export class LoginDashboardComponent implements OnInit {

  constructor(private service: AppService, private router: Router) { }

  ngOnInit(): void {}

  login(empId: any, password: number): void {
    this.service.getUsers(empId).subscribe(res => {
      if (res) {
        if (res.password === Number(password)) {
          this.service.setUserData(res);
          sessionStorage.setItem('LoginUserName', res.firstName);
          this.router.navigateByUrl('/userDashboard');
        }
        else {
          alert('wrong Password');
        }
      }
    }, error => {
      alert('something went wrong');
    });
  }

}
