import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/service/app.service';
import { UserModel } from './userdata.module'
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {

  displayedColumns: string[] = ['srno', 'name', 'address', 'mobile','email', 'Action'];
  dataSource: any;
  constructor(private apiservice: AppService, private route: Router, private activatedRoute: ActivatedRoute, public dialog: MatDialog,private router: Router) { }

  userModelObj: UserModel = new UserModel();
  public employeeData: any;
  public showAdd: boolean;
  public showaUpdate: boolean;
  public firstname: any;
  public userData: any;

  ngOnInit(): void {
    this.userData = this.activatedRoute.snapshot.data.savedUserData;
    this.getallusers()
  }

  getallusers() {
    this.apiservice.getUsers().subscribe((res: any) => {
      this.dataSource = res;
    });
  }

  openDialog() {
    this.dialog.open(DialogComponent, {
      width: '30%'
    }).afterClosed().subscribe(val=>{
      if(val === 'Save'){
        this.getallusers();
      }
    });
  }

  onEdit(row: any) {
    console.log("Event", Event)
    this.dialog.open(DialogComponent, {
      width: '30%',
      data: row
    }).afterClosed().subscribe(val=>{
      if(val === 'Update'){
        this.getallusers();
      }
    });
  }

  onDelete(row: any) {
    debugger
    this.apiservice.deleteUser(row.id).subscribe(res => {
      this.getallusers()
    }, error => {
      alert('something went wrong')
    });
  }

  logout(){
    this.apiservice.logOutUser();
    this.router.navigate(['login'])
  }

}
