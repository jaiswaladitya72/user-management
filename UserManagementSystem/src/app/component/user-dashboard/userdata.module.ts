export class UserModel{
    id:number = 0;
    firstName : string = '';
    email : string =""
    address : string = '';
    mobile : string = '';
    role: string = '';
    password : string = '';
}