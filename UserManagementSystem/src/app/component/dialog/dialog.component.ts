import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  userForm !: FormGroup;
  public actionBtn = 'Save';

  constructor(private formBuilder : FormBuilder, private apiservice : AppService, private dialogRef : MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) private editData : any ) { }

  ngOnInit(): void {
    this.userForm =this.formBuilder.group({
      firstName: ['',Validators.required],
      email: ['',Validators.required],
      password: ['',Validators.required],
      address: ['',Validators.required],
      mobile: ['',Validators.required],
      role: ['',Validators.required]
    })
    if(this.editData){
      this.actionBtn = 'Update'
      this.userForm.controls['firstName'].setValue(this.editData.firstName)
      this.userForm.controls['email'].setValue(this.editData.email)
      this.userForm.controls['password'].setValue(this.editData.password)
      this.userForm.controls['address'].setValue(this.editData.address)
      this.userForm.controls['mobile'].setValue(this.editData.firstName)
      this.userForm.controls['role'].setValue(this.editData.role)

    }
  }

  addUser(){
    if(!this.editData){
      this.apiservice.postUser(this.userForm.value).subscribe(res=>{
        alert('added suceess');
        this.userForm.reset();
        this.dialogRef.close('Save');
      },error=>{
        alert('something went wrong');
      });
    }
    else{
      this.apiservice.updateUser(this.userForm.value,this.editData.id).subscribe(res=>{
        alert("Update suceess")
        this.userForm.reset();
        this.dialogRef.close('Update');
      });
    }
  }

  closePopup(){
    this.dialogRef.close();
  }

}
