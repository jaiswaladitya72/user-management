import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginDashboardComponent } from './component/login-dashboard/login-dashboard.component';
import { UserDashboardComponent } from './component/user-dashboard/user-dashboard.component';
import { ResolveGuard } from './guards/resolve.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginDashboardComponent },
  { path: 'userDashboard', component: UserDashboardComponent, resolve: {savedUserData: ResolveGuard} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
